package glowna;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class menu extends JFrame implements ActionListener{

		public int szerokosc=800;
		public int wysokosc=600;
		JButton b1,b2,b3;
		
		menu(){
			
			setSize(szerokosc, wysokosc);
			setLocationRelativeTo(null);
			setTitle("menu");
			
			b1=new JButton("nowa gra");
			b2=new JButton("tabela wynikow");
			b3=new JButton("wyjscie");
			
			b1.setBounds(szerokosc/2-75,wysokosc/4,150,20);
			b2.setBounds(szerokosc/2-75, wysokosc/3, 150, 20);
			b3.setBounds(szerokosc/2-75, wysokosc/2, 150, 20);
			
			b1.addActionListener(this);
			b2.addActionListener(this);
			b3.addActionListener(this);
			
			setLayout(null);
			add(b1);
			add(b2);
			add(b3);
			
			
		}
		public static void main(String[] args){
			menu gl=new menu();
			gl.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			gl.setVisible(true);
		}
		
		public void actionPerformed(ActionEvent e) {
			Object source=e.getSource();
			if(source==b1){
				Ramka ram=new Ramka();
				this.dispose();
		}
			else if (source==b3){
				System.exit(0);
			}
}
}