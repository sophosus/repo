package glowna;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.*;
import javax.swing.JFrame;

public class Ramka extends JFrame {
	
	public static Dimension rozm = new Dimension(800, 600);
	public Ramka(){
		
		new JFrame();
	
		setTitle("Tower Defense");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(rozm);
		setLocationRelativeTo(null);
		setResizable(false);
		
		inicjalizuj();
	}
	public void inicjalizuj(){
		setLayout(new GridLayout(1,1,0,0));
		Ekran ekran=new Ekran(this);
		add(ekran);
		
		setVisible(true);
	}
	/*public static void main(String [] args){
		menu gl=new menu();
		Ramka ramka=new Ramka();
	}*/
}
