package glowna;

import java.awt.*;

public class Swiat {
	public int Ilosc_szer=27;
	public int Ilosc_wys=19;
	public static int RozmKratki=26;
	public Block[][] kratka;
	
	public Swiat(){
		define();
	}
	public void define()
	{
		kratka=new Block[Ilosc_wys][Ilosc_szer];
		for(int i=0;i<kratka.length;i++){
			for (int j=0;j<kratka[0].length;j++)
			{
				kratka[i][j]=new Block((Ekran.szer/2)-(Ilosc_szer*RozmKratki/2)+j*RozmKratki,i*RozmKratki,RozmKratki,RozmKratki,Tekstury.trawa,Tekstury.puste);
			}
		}
	}
	public void physic(){
		for (int i=0;i<kratka.length;i++)
		{
			for(int j=0;j<kratka[0].length;j++)
			{
				kratka[i][j].physic();
			}
		}
	}
	
	public void draw(Graphics g){
		
		for(int i=0;i<kratka.length;i++){
			for (int j=0;j<kratka[0].length;j++)
			{
				kratka[i][j].draw(g);
			}
		}
		for(int i=0;i<kratka.length;i++){
			for (int j=0;j<kratka[0].length;j++)
			{
				kratka[i][j].strzal(g);
			}
		}
	}
}
