package glowna;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;



public class Block extends Rectangle {
	private static boolean isFirst = true;
	public static int zasieg = 110; 
	public Rectangle zasiegKwadrat;
	public int IDziemi;
	public int IDnad;
	public int loseTime = 80, loseFrame = 0;
	public int obr=5;
	
	
	public int shotMob = -1;
	public boolean strzela = false;
	
	public Block(int x, int y, int width, int height, int IDziemi, int IDnad) {
		if(isFirst) {
			isFirst = false;
			
		}
		setBounds(x, y, width, height);
		zasiegKwadrat = new Rectangle(x - (zasieg/2), y - (zasieg/2), width + zasieg, height + zasieg);
		this.IDziemi = IDziemi;
		this.IDnad = IDnad;
	}
	
	public void draw(Graphics g) {
		g.drawImage(Ekran.ziemia[IDziemi], x, y, width, height, null);
		
		if(IDnad != Tekstury.puste) {
			g.drawImage(Ekran.air[IDnad], x, y, width, height, null);
		}
	}
	
	public void physic() {
		if (shotMob != -1 && zasiegKwadrat.intersects(Ekran.wrogowie[shotMob])) {
			strzela = true;
		} else {
			strzela = false;
		}
		
		if(!strzela) {

			if (IDnad == Wieze.laserowa) {
				for (int i = 0; i < Ekran.wrogowie.length; i++) {
					if (Ekran.wrogowie[i].wGrze) {
						if (zasiegKwadrat.intersects(Ekran.wrogowie[i])) {
							shotMob = i;
						}
					}
				}
			}
		}	
		
		if(strzela) {
			if(loseFrame >= loseTime) {
				
				Ekran.wrogowie[shotMob].Dmg(obr);
				if(Gracz.zabici%10==1)
				{
					if(obr>2){
						obr-=1;
					}
					
				}
				loseFrame = 0;
			} else {
				loseFrame += 1;
			}
			
			if(Ekran.wrogowie[shotMob].isdead()) {
				//Gracz.getmoney(Ekran.wrogowie[shotMob].wrogID);
				strzela = false;
				shotMob = -1;
				
				//Ekran.hasWon();
			}	
		}
	}

	
	
	public void strzal(Graphics g) {
		
		if(strzela) {
			g.setColor(Color.YELLOW);
			g.drawLine(x + (width/2), y + (height/2), Ekran.wrogowie[shotMob].x + (Ekran.wrogowie[shotMob].width/2), Ekran.wrogowie[shotMob].y + (Ekran.wrogowie[shotMob].height/2));
			
		}
	}
}