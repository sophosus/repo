package glowna;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.*;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.*;

import javax.swing.*;
public class Ekran extends JPanel implements Runnable{
	
	public Thread thread=new Thread(this);
	public static boolean poczatek=true;
	public static boolean czyZabity=false;
	public static int szer,wys;
	
	
	public static Image[] ziemia=new Image[10];
	public static Image[] air=new Image[10];
	public static Image[] res=new Image[10];
	public static Image[] wrog=new Image[10];
	public static Point mpos=new Point(0,0);

	
	public static Swiat swiat;
	public static Sklep sklep;
	public static InOut save;
	public static Wrog[] wrogowie=new Wrog[40];
	
	public Ekran(Ramka ramka){
		ramka.addMouseListener(new Myszka());
		ramka.addMouseMotionListener(new Myszka());
		thread.start();
		}
	public void define(){
		swiat=new Swiat();
		save=new InOut();
		sklep=new Sklep();
		
		
		for (int i=0;i<ziemia.length;i++){
			ziemia[i]=new ImageIcon("resources/trawa1.png").getImage();
			ziemia[i]=createImage(new FilteredImageSource(ziemia[i].getSource(),new CropImageFilter(0,26*i,26,26)));
		}
		air[0]=new ImageIcon("resources/zamek.png").getImage();
		for (int i=1;i<air.length;i++)
		{
			air[i]=new ImageIcon("resources/nakladane.png").getImage();
			air[i]=createImage(new FilteredImageSource(air[i].getSource(),new CropImageFilter(0,26*i-26,26,26)));
		}
		res[0]=new ImageIcon("resources/res.png").getImage();
		res[1]=new ImageIcon("resources/health.png").getImage();
		res[2]=new ImageIcon("resources/coin.png").getImage();
		
		wrog[0]=new ImageIcon("resources/potwor1.png").getImage();
		
		save.loadSave(new File("zapis/level1.lv"));
		
		for (int i=0;i<wrogowie.length;i++)
		{
			wrogowie[i]=new Wrog();
			
		}
	}
	
	public void paintComponent(Graphics g)
	{
		if(poczatek){
			szer=getWidth();
			wys=getHeight();
			define();
			poczatek=false;
		}
		g.setColor(new Color(50,50,50));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		
		swiat.draw(g);
		sklep.draw(g);
		for (int i=0;i<wrogowie.length;i++)     // ilosc wrogow
		{
			if(wrogowie[i].wGrze)
			{
				wrogowie[i].draw(g);
			}
		}
		
		if(Gracz.hp<1){
			g.setColor(Color.RED);
			g.fillRect(0, 0, szer, wys);
			g.setColor(new Color(255,255,255));
			g.setFont(new Font("Times New Roman",Font.BOLD,16));
			g.drawString("Koniec gry", szer/2-50, wys/2);
			
		}
	}
	
	public static int czasspawn=300,spawncounter=0;;
	public void spawner(){
		if(spawncounter>czasspawn)
		{
			for (int i=0;i<wrogowie.length;i++)
			{
				if(!wrogowie[i].wGrze)
				{
					wrogowie[i].spawn(Wrog.zielony);
					break;
				}
			}
			spawncounter=0;
		}
		else{
			spawncounter+=1;
		}
	}
	
	public void run() {
		while(true){
			if (!poczatek&&Gracz.hp>0){
				swiat.physic();
				spawner();
				for (int i=0;i<wrogowie.length;i++)
				{
					if(wrogowie[i].wGrze)
					{
						wrogowie[i].move();
					}
				}
			}
			
			
			repaint();
			
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
	}
}