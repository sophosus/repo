package glowna;
import java.awt.*;

public class Wrog extends Rectangle{
	public static int wrogAir=-1;
	public static int zielony=0;
	
	public int gora=0,dol=1,prawo=2,lewo=3;
	public int kierunek=prawo;
	public int rWrog=26;
	public int wrogID=wrogAir;
	protected boolean wGrze=false;
	public int xwrog,ywrog;
	public int wrogidzie=0;
	public int wrogszybkosc=0,wrogruch;
	protected int hp;
	public static int maxhp=26;
	public int healthspace=3,healthheight=2;

	public Wrog(){
		
	}
	public void spawn(int ID)
	{
		for(int i=0;i<Ekran.swiat.kratka.length;i++)
		{
			if(Ekran.swiat.kratka[i][0].IDziemi==Tekstury.droga){
				setBounds(Ekran.swiat.kratka[i][0].x,Ekran.swiat.kratka[i][0].y,rWrog,rWrog);
				xwrog=0;
				ywrog=i;
				
				kierunek = prawo;
				hp = maxhp;
				
				break;
			}
		}
		
		this.wrogID=ID;
		
		wGrze=true;
	}
	public void umiera(){
		wGrze=false;
		kierunek=prawo;
		wrogidzie=0;
	}
	public void stratazycia()
	{
		Gracz.hp-=1;
	}
	public void move(){
		if(wrogruch>wrogszybkosc){
			if(kierunek==prawo)
			{
				x+=1;
			}
			else if(kierunek==lewo)
			{
				x-=1;
			}
			else if (kierunek==dol)
			{
				y+=1;
			}
			else if (kierunek==gora)
			{
				y-=1;
			}
			wrogidzie+=1;
			if (wrogidzie==Ekran.swiat.RozmKratki)
			{
				if(kierunek==prawo)
				{
					xwrog+=1;
				}
				else if(kierunek==lewo)
				{
					xwrog-=1;
				}
				else if (kierunek==dol)
				{
					ywrog+=1;
				}
				else if (kierunek==gora)
				{
					ywrog-=1;
				}
				try{
				if(Ekran.swiat.kratka[ywrog+1][xwrog].IDziemi==Tekstury.droga){
					if (kierunek==gora)
					{
						if (Ekran.swiat.kratka[ywrog-1][xwrog].IDziemi!=Tekstury.droga){
							
							if(Ekran.swiat.kratka[ywrog][xwrog+1].IDziemi==Tekstury.droga){	
							kierunek=prawo;
							}
							else if(Ekran.swiat.kratka[ywrog][xwrog-1].IDziemi==Tekstury.droga)
							{
								kierunek=lewo;
							}
						}
					}
				else{
						kierunek=dol;
					}
					
				}
				else if(Ekran.swiat.kratka[ywrog-1][xwrog].IDziemi==Tekstury.droga&&Ekran.swiat.kratka[ywrog][xwrog+1].IDziemi!=Tekstury.droga){
					if(kierunek==prawo){
						kierunek=gora;
					}
					if (kierunek==dol){
						kierunek=lewo;
					}
					
				}
				else if (kierunek==lewo)
				{
					if(Ekran.swiat.kratka[ywrog-1][xwrog].IDziemi==Tekstury.droga){
						kierunek=gora;
					}
					else if (Ekran.swiat.kratka[ywrog+1][xwrog].IDziemi==Tekstury.droga){
						kierunek=dol;
					}
				}
				
				else if(Ekran.swiat.kratka[ywrog][xwrog+1].IDziemi==Tekstury.droga){
					if(kierunek!=lewo){
						kierunek=prawo;
					}
				}
				
				
				}catch(Exception e){}
				wrogidzie=0;
			}
			if (Ekran.swiat.kratka[ywrog][xwrog].IDnad==Tekstury.zamek)
			{
				stratazycia();
				umiera();
				
			}
			wrogruch=1;
		}
		else{
			wrogruch+=1;
		}
	}
	public void Dmg(int dmg)
	{
		hp-=dmg;
		czyumiera();
	}
	public void czyumiera (){
		if(hp<=0)
		{
			umiera();
			Gracz.zabici++;
			if(Ekran.czasspawn>50){
			Ekran.czasspawn-=Gracz.zabici;
			Gracz.hajs+=5;
			
			}
			Gracz.Wynik();
			//System.out.println(Ekran.czasspawn);
		}
	}
	public boolean isdead()
	{
		if(wGrze)
		{
			return false;
		}
		else{
			return true;
		}
	}
	public void draw(Graphics g)
	{
		g.drawImage(Ekran.wrog[wrogID], x, y, width, height,null);
		g.setColor(Color.black);
		g.fillRect(x,y-healthspace+healthheight, width, healthheight);
		
		g.setColor(Color.red);
		g.fillRect(x,y-healthspace+healthheight, hp, healthheight);
	}
	
}
