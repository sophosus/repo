package glowna;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;



public class Sklep {
	public static int shopWidth=8;
	public static int buttonSize=32;
	public Rectangle[] button=new Rectangle[shopWidth];
	public Rectangle bHealth;
	public Rectangle bCoins;
	public static int centerItem=4;
	public static int[] buttonID={Wieze.laserowa,Tekstury.puste,Tekstury.puste,Tekstury.puste,Tekstury.puste,Tekstury.puste,Tekstury.puste,Tekstury.kosz};
	public static int wRece;
	public static int wybranawieza;
	public boolean coswRece=false;
	public static int[] cenawiezy={10,0,0,0,0,0,0,0};
	
	public Sklep(){
		define();
	}
	public void klikniecie(int mouseClick)
	{
		if(mouseClick==3)
		{
			coswRece=false;
		}
		if(mouseClick==1)
		{
			for (int i=0;i<button.length;i++)
			{
				if (button[i].contains(Ekran.mpos))
				{
					if(buttonID[i]!=Tekstury.puste)
					{
					if(buttonID[i]==Tekstury.kosz)
					{
						coswRece=false;
					}
					else{
					wRece=buttonID[i];
					wybranawieza=i;
					coswRece=true;
					}
					}
				}
			}
			if(coswRece){
				if(Gracz.hajs>=cenawiezy[wybranawieza]){
					for(int i=0;i<Ekran.swiat.kratka.length;i++)
					{
						for (int j=0;j<Ekran.swiat.kratka[0].length;j++)
						{
							if(Ekran.swiat.kratka[i][j].contains(Ekran.mpos)){
								if(Ekran.swiat.kratka[i][j].IDziemi!=Tekstury.droga&&Ekran.swiat.kratka[i][j].IDnad==Tekstury.puste&&Ekran.swiat.kratka[i][j].IDziemi==3)
								{
									Ekran.swiat.kratka[i][j].IDnad = wRece;
									Gracz.hajs-=cenawiezy[wybranawieza];
								}
							}
						}
					}
				}
			}
				
		}
	}
	public void define(){
		for (int i=0;i<button.length;i++)
		{
			button[i]=new Rectangle(Ekran.szer/2-(shopWidth*(buttonSize+2))/2+(buttonSize+2)*i,Ekran.wys-Ekran.wys/9,buttonSize,buttonSize);
		}
		bHealth=new Rectangle(Ekran.swiat.kratka[0][4].x,button[0].y,25, 25);
		bCoins=new Rectangle(Ekran.swiat.kratka[0][1].x,button[0].y,25, 25);
	}
	public void draw(Graphics g)
	{
		
		g.setColor(Color.black);
		g.drawImage(Ekran.res[1],bHealth.x, bHealth.y, bHealth.width,bHealth.height,null);
		g.drawImage(Ekran.res[2],bCoins.x, bCoins.y, bCoins.width,bCoins.height,null);
		for (int i=0;i<button.length;i++)
		{
			if(button[i].contains(Ekran.mpos)){
				if(cenawiezy[i]>Gracz.hajs||buttonID[i]==-1)
				{
				g.setColor(Color.red);
				
				g.fillRect(button[i].x, button[i].y, button[i].width, button[i].height);
				}
			}
			g.drawImage(Ekran.res[0],button[i].x, button[i].y, button[i].width, button[i].height,null);
			if (buttonID[i]!=Tekstury.puste)g.drawImage(Ekran.air[buttonID[i]], button[i].x, button[i].y, button[i].width, button[i].height,null);
			g.setColor(Color.white);
			if(cenawiezy[i]>0)g.drawString(cenawiezy[i]+"", button[i].x+button[i].height/3, button[i].y+button[i].width/2);
		}
		g.setColor(Color.white);
		g.setFont(new Font("Times New Roman",Font.BOLD,14));
		g.drawString(""+Gracz.hp, bHealth.x-bHealth.width, bHealth.y+bHealth.height/3*2);
		g.drawString(""+Gracz.hajs, bCoins.x-bCoins.width, bCoins.y+bCoins.height/3*2);
		if(coswRece)
		{
			g.drawImage(Ekran.air[wRece], Ekran.mpos.x - ((button[0].width - (centerItem*2))/2) + centerItem, Ekran.mpos.y - ((button[0].width - (centerItem*2))/2) + centerItem, button[0].width - (centerItem*2), button[0].height - (centerItem*2), null);
		}
	}
}
