package glowna;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.*;


public class Myszka implements MouseMotionListener, MouseListener{

	public void mouseClicked(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}

	public void mousePressed(MouseEvent e) {
		Ekran.sklep.klikniecie(e.getButton());
	}

	public void mouseDragged(MouseEvent e) {
		Ekran.mpos = new Point(e.getX() - ((Ramka.rozm.width - Ekran.szer) / 2), e.getY() - (Ramka.rozm.height - Ekran.wys - (Ramka.rozm.width - Ekran.szer) / 2));
	}


	public void mouseMoved(MouseEvent e) {
		Ekran.mpos = new Point(e.getX() - ((Ramka.rozm.width - Ekran.szer) / 2), e.getY() - (Ramka.rozm.height - Ekran.wys - (Ramka.rozm.width - Ekran.szer) / 2));
	}
	public void mouseExited(MouseEvent e) {}

	public void mouseReleased(MouseEvent e) { }

}
